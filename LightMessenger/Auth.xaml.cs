﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VkNet;
using VkNet.Enums.Filters;

namespace LightMessenger
{
    /// <summary>
    /// Interaction logic for Auth.xaml
    /// </summary>
    public partial class Auth : Window
    {
        VkApi _vkApi;
        Window _caller;

        public Auth(VkApi vkApi, Window caller)
        {
            _caller = caller;
            _vkApi = vkApi;
            InitializeComponent();
        }

        private void PrintError(string error)
        {
            label_enter_vk.Content = error;
            label_enter_vk.Foreground = Brushes.PaleVioletRed;
        }

        private void button_autorize_Click(object sender, RoutedEventArgs e)
        {
            label_enter_vk.Content = "Думаю...";
            label_enter_vk.Foreground = Brushes.Black;

            try {
                _vkApi.Authorize(new ApiAuthParams
                {
                    ApplicationId = 5786604,
                    Login = textBox_login.Text,
                    Password = textBox_pass.Text,
                    Settings = Settings.All
                });
            }
            catch (Exception exception)
            {
                PrintError(exception.ToString());
            }

            if (_vkApi.IsAuthorized)
            {
                this.Hide();
                _caller.Show();
            }
            else
            {
                PrintError("Ошибка авторизации ВК");
            }

        }


        private void Window_Closed(object sender, EventArgs e)
        {
            _caller.Close();
        }

        private void textBox_login_GotFocus(object sender, RoutedEventArgs e)
        {
            textBox_login.Text = "";
            textBox_login.Foreground = Brushes.Black;
        }

        private void textBox_pass_GotFocus(object sender, RoutedEventArgs e)
        {
            textBox_pass.Text = "";
            textBox_pass.Foreground = Brushes.Black;
        }

        private void onKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                button_autorize_Click(this, e);
            }

        }

    }
}
