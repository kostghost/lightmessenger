﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace LightMessenger.Tools
{
    public class VisualTools
    {
        /// <summary>
        /// Function for searching ScrollViewer object from any ListBox
        /// </summary>
        /// <param name="listBox"></param>
        /// <returns></returns>
        public static ScrollViewer GetScrollViewer(ListBox listBox)
        {
            Border scroll_border = VisualTreeHelper.GetChild(listBox, 0) as Border;
            if (scroll_border is Border)
            {
                ScrollViewer scroll = scroll_border.Child as ScrollViewer;
                if (scroll is ScrollViewer)
                {
                    return scroll;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
