﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Model.RequestParams;

namespace LightMessenger.Areas.LongPool
{
    public class LongPoolArea
    {
        //ЛонгПул Сервер
        VkNet.Model.LongPollServerResponse longPollServer;
        VkApi vkApi;
        Friends.FriendsArea friendsArea;
        Messages.MessagesArea messagesArea;

        //VkNet.Model.LongPollHistoryResponse poolCache;

        public void Initialize(VkApi vkApi, Friends.FriendsArea friendsArea, Messages.MessagesArea messagesArea)
        {
            this.vkApi = vkApi;
            this.friendsArea = friendsArea;
            this.messagesArea = messagesArea;
            //Пытаемся подклдючиться к ЛонгПул Серверу
            longPollServer = vkApi.Messages.GetLongPollServer(false, true);
        }

        public void LoadUpdatesFromServer()
        {
            VkNet.Model.LongPollHistoryResponse getLongPollHistory = null;

            //TODO выставить обновление в отдельный поток
            getLongPollHistory = vkApi.Messages.GetLongPollHistory(new MessagesGetLongPollHistoryParams
            {
                Ts = longPollServer.Ts,
                Pts = longPollServer.Pts,
                EventsLimit = 1000,
            });

            longPollServer.Pts = getLongPollHistory.NewPts;

            ChainHandling(getLongPollHistory);
        }

        //Без паттерна, конечно, обработка пришедших сообщений
        void ChainHandling(VkNet.Model.LongPollHistoryResponse longPollHistory)
        {
            messagesArea.LoadFreshMessages(longPollHistory);
            friendsArea.LongPoolHandler(longPollHistory);
        }
    }
}
