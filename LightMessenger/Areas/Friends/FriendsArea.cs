﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model.RequestParams;

namespace LightMessenger.Areas.Friends
{
    public class FriendsArea
    {
        VkApi vkApi;
        long? choosenId; //TODO Убрать в ивент

        //Храним инфу о друзьях
        VkNet.Utils.VkCollection<VkNet.Model.User> myFriends;

        //Отформатированный список пользовтателей тут
        ObservableCollection<UserBox> listOfUserBox = new ObservableCollection<UserBox>();
        ListBox listBox_FriendsList;


        public void Initialize(VkApi vkontakteDotNetApi, ListBox friendsListBox)
        {
            vkApi = vkontakteDotNetApi;
            listBox_FriendsList = friendsListBox;
        }

        /// <summary>
        /// Обновляет список друзей, друг с последним сообщением вверху. Также показывает количество новых сообщений
        /// </summary>
        /// <param name="longPollHistory"></param>
        public void LongPoolHandler(VkNet.Model.LongPollHistoryResponse longPollHistory)
        {
            foreach (var ev in longPollHistory.History)
            {
                if((int)ev.ElementAt(0) == 4) //Пришло новое сообщение
                {
                    var recieverId = (long?)ev.ElementAt(3);
                    AddUnreadMessagesCount(recieverId, 1);
                }
                else if ((int)ev.ElementAt(0) == 6) //Прочтение всех входящих сообщений с $peer_id вплоть до $local_id включительно.
                {
                    var recieverId = (long?)ev.ElementAt(1);
                    AddUnreadMessagesCount(recieverId, 0);
                }
            }
        }

        //При msgCount == 0 сброс счетчика
        void AddUnreadMessagesCount(long? vkId, int msgCount)
        {
            //Нет ли пропуска сообщения при удалении/добавлении??
            for (int i = 0; i < listOfUserBox.Count; i++)
            {

                if (listOfUserBox.ElementAt(i).VkID == vkId)
                {
                    var oldBox = listOfUserBox.ElementAt(i);
                    listOfUserBox.RemoveAt(i);

                    if (msgCount == 0)
                        oldBox.NewMessagesCount = 0;
                    else 
                        oldBox.NewMessagesCount = oldBox.NewMessagesCount + msgCount;

                    //TODO можно ускорить, если писать Add вместо Insert
                    listOfUserBox.Insert(0, oldBox);
                }
            }
        }

        /// <summary>
        /// Обновляет список друзей
        /// </summary>
        public void UpdateFriends()
        {
            myFriends = vkApi.Friends.Get(new FriendsGetParams
            {
                Count = 30,
                Fields = ProfileFields.Online | ProfileFields.LastName | ProfileFields.FirstName | ProfileFields.Photo50,
                Order = VkNet.Enums.SafetyEnums.FriendsOrder.Hints
            });

            //Добавим сначала себя :D
            var me = vkApi.Users.Get((long)vkApi.UserId, ProfileFields.Online | ProfileFields.LastName | ProfileFields.FirstName | ProfileFields.Photo50);
            listOfUserBox.Add(new UserBox(me));

            //Добавляем каждого юзера в список слева
            foreach (var friend in myFriends)
            {
                listOfUserBox.Add(new UserBox(friend));
            }
        }

        //TODO Убрать в ивент
        public long? GetChoosenFriendId()
        {
            var choosenIndex = listBox_FriendsList.SelectedIndex;

            choosenId = listOfUserBox.ElementAt(choosenIndex).VkID;

            return choosenId;
        }

        public ObservableCollection<UserBox> GetListOfFriendsBoxes()
        {
            return listOfUserBox;
        }

    }
}
