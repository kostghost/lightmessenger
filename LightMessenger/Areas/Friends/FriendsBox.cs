﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightMessenger.Areas.Friends
{
    /// <summary>
    /// Класс для формирования списка пользователей слева
    /// </summary>
    public class UserBox
    {

        public UserBox(VkNet.Model.User user)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            IsOnline = user.Online;
            PhotoPatch = user.Photo50;

            VkID = user.Id;
            NewMessagesCount = 0;
        }

        public Uri PhotoPatch { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public bool? IsOnline { get; }
        public long? VkID { get; }
        public int NewMessagesCount { get; set; }

    }
}
