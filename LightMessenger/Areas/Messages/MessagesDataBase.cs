﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightMessenger.Areas.Messages
{
    //TODO add exceptions
    public class MessagesDataBase
    {
        Dictionary<long?, List<VkNet.Model.Message>> _messagesForIds;
        Dictionary<long?, int> _offsetForIds;

        public MessagesDataBase()
        {
            _messagesForIds = new Dictionary<long?, List<VkNet.Model.Message>>();
            _offsetForIds = new Dictionary<long?, int>();
        }

        public bool Add(long? vkId, ReadOnlyCollection<VkNet.Model.Message> messagesList, int offset = 0)
        {
            var newList = new List<VkNet.Model.Message>(messagesList);
            
            newList.Reverse();
            //TODO Понятное дело, нужно как-то обновлять этот список
            if (_messagesForIds.ContainsKey(vkId))
            {
                _messagesForIds[vkId].AddRange(newList);
            }
            else
                _messagesForIds.Add(vkId, newList);

            if (_offsetForIds.ContainsKey(vkId))
            {
                _offsetForIds[vkId] = offset; //TODO
            }
            else
                _offsetForIds.Add(vkId, offset);

            return true;
        }

        public bool SetOffset(long? vkId, int offset)
        {
            _offsetForIds[vkId] = offset;

            return true;
        }

        public List<VkNet.Model.Message> GetMessagesList(long? vkId)
        {
            if (_messagesForIds.ContainsKey(vkId))
                return _messagesForIds[vkId];
            else
                return null;
        }

        public int GetOffset(long? vkId)
        {
            if (_offsetForIds.ContainsKey(vkId))
                return _offsetForIds[vkId];
            else
                return 0; //костыль, в принципе
        }

    }
}
