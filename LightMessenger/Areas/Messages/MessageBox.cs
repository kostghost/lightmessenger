﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightMessenger.Areas.Messages
{
    /// <summary>
    /// Класс для формирования списка сообщений
    /// </summary>
    public class MessageBox
    {
        private string DateToTimeStr(System.DateTime? date)
        {
            if (date == null)
                return "";
            return string.Format("{0}:{1}", date.Value.Hour, date.Value.Minute);
        }

        public MessageBox(VkNet.Model.Message message)
        {
            Body = message.Body;
            Time = DateToTimeStr(message.Date);
            Type = message.Type;
            if (Type == VkNet.Enums.MessageType.Sended)
                IsItSendedMessage = true;
        }

        public string Body { get; }
        public string Time { get; }
        public VkNet.Enums.MessageType? Type { get; }
        public bool IsItSendedMessage { get; }
    }
}
