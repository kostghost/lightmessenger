﻿using LightMessenger.Tools;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media;
using VkNet;
using VkNet.Model.RequestParams;

namespace LightMessenger.Areas.Messages
{
    public class MessagesArea
    {
        bool isDebug = true;
        const string EmptyMessageText = "Write a message...";
        VkApi vkApi;
        MessagesDataBase messagesDataBase = new MessagesDataBase(); 

        //А тут храним список мессаджбоксов для конкретного юзера, именно он обрабатывается в listBox
        ObservableCollection<MessageBox> listOfMessageBox = new ObservableCollection<MessageBox>();

        //ссылка на скролвьювер оф listView_messages
        ScrollViewer scrollViewer; 

        //Ссылка на поле с мессаджами.
        ListBox listBox_messages;
        //Ссылка на поле ввода сообщения
        TextBox textBox_WriteAMessage;

        LongPool.LongPoolArea longPoolArea;

        long? chosenId; //TODO Сделать ивент!!!
        public void SetChoosenId(long? id)
        {
            chosenId = id;
        }

        public void Initialize(VkApi vkontakteDotNetApi, LongPool.LongPoolArea longPoolArea, TextBox writeAMessageTextBox, ListBox listBox_messages)
        {
            textBox_WriteAMessage = writeAMessageTextBox;
            vkApi = vkontakteDotNetApi;
            this.listBox_messages = listBox_messages;
            this.longPoolArea = longPoolArea;
        }

        public static void OnLostFocusSetStyle(TextBox textBox_message)
        {
            if (textBox_message.Text == "")
            {
                textBox_message.Text = EmptyMessageText;
                textBox_message.Foreground = Brushes.Gray;
            }
        }
        public static void OnGotFocusSetStyle(TextBox textBox_message)
        {
            textBox_message.Text = "";
            textBox_message.Foreground = Brushes.Black;
        }

        public ObservableCollection<MessageBox> GetListOfMessageBoxes()
        {
            return listOfMessageBox;
        }

        /// <summary>
        /// Sends a message from assigned TextBox
        /// </summary>
        public void SendMessageFromBox()
        {
            if (textBox_WriteAMessage.Text != "" && textBox_WriteAMessage.Text != EmptyMessageText)
            {
                var send = vkApi.Messages.Send(new MessagesSendParams
                {
                    UserId = chosenId,
                    Message = textBox_WriteAMessage.Text
                });
            }
            textBox_WriteAMessage.Text = "";


            longPoolArea.LoadUpdatesFromServer();
        }


        /// <summary>
        /// Loads messages were written before app started from VK 
        /// </summary>
        public void LoadOldMessages(long? vkId)
        {
            if (isDebug && messagesDataBase.GetOffset(vkId) == 0)   //TODO Убрать
            {
                uint messagesToGet = 50;


                var dialogMessages = vkApi.Messages.GetHistory(new MessagesGetHistoryParams
                {
                    Count = messagesToGet,
                    Offset = messagesDataBase.GetOffset(vkId),
                    UserId = vkId                    
                });

                //TODO исправить messagesToGet
                messagesDataBase.Add(vkId, dialogMessages.Messages, (int)messagesToGet);
                //TODO УВЕЛИЧИВАЕМ!!!! Увеличиваем оффсет для следующего считывания сообщений
                messagesDataBase.SetOffset(vkId, (int)messagesToGet);
            }
        }

        //Loads a new messages written after app started
        public void LoadFreshMessages(VkNet.Model.LongPollHistoryResponse longPollHistory, long? vkId = null)
        {
            if (longPollHistory.Messages != null && longPollHistory.Messages.Count > 0)
            {
                if (vkId == null)
                    vkId = chosenId;
                if (chosenId != null)
                {
                    var currentCountOfMessages = messagesDataBase.GetMessagesList(vkId).Count;
                    var lastLoadedMessage = messagesDataBase.GetMessagesList(vkId)[currentCountOfMessages - 1];

                    messagesDataBase.Add(vkId, longPollHistory.Messages);
                    UpdateMessagesJustAddedNew(vkId);
                    
                }
            }
        }

        /// <summary>
        /// Update a ListWiew for chosen user id
        /// </summary>
        /// <param name="vkId"></param>
        public void UpdateMessagesFullUpdate(long? vkId = null, bool isJustAddedNewMessages = false)
        {
            if (messagesDataBase.GetMessagesList(vkId) == null)
                LoadOldMessages(vkId);

            if (vkId == null)
                vkId = chosenId;

            listOfMessageBox.Clear();
            //Обновляем отслеживаемый список
            foreach (var message in messagesDataBase.GetMessagesList(vkId))
            {
                if (message.UserId == chosenId)
                {
                    var msgBoxItem = new MessageBox(message);
                    listOfMessageBox.Add(msgBoxItem);
                }
                //textBox.Text += message.Date + ": " +  message.Body + "\n";
            }

            //находим скролвьювер TODO перенести бы куда-нибудь, чтоб не вызывать кажлый раз
            scrollViewer = VisualTools.GetScrollViewer(listBox_messages);
            scrollViewer.ScrollToBottom();
        }

        /// <summary>
        /// Add a new loaded messages to ListWiew for chosen user id
        /// </summary>
        /// <param name="vkId"></param>
        public void UpdateMessagesJustAddedNew(long? vkId = null)
        {
            if (messagesDataBase.GetMessagesList(vkId) == null)
                LoadOldMessages(vkId);

            if (vkId == null)
                vkId = chosenId;

            for (int i = listOfMessageBox.Count; i < messagesDataBase.GetMessagesList(vkId).Count; i++)
            {
                //Не помню, зачем эта проверка O_o
                if (messagesDataBase.GetMessagesList(vkId)[i].UserId == chosenId)
                {
                    var msgBoxItem = new MessageBox( messagesDataBase.GetMessagesList(vkId)[i] );
                    listOfMessageBox.Add(msgBoxItem);
                }
            }

            //находим скролвьювер TODO перенести бы куда-нибудь, чтоб не вызывать кажлый раз
            scrollViewer = VisualTools.GetScrollViewer(listBox_messages);
            scrollViewer.ScrollToBottom();
        }


    }
    

}
