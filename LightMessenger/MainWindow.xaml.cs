﻿using LightMessenger.Areas.Friends;
using LightMessenger.Areas.LongPool;
using LightMessenger.Areas.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model.RequestParams;

namespace LightMessenger
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Window authWindow;
        VkApi vkApi = new VkApi();
        bool isDebug = false;   //DEBUG
       
        long? choosenId; //а тут храним как раз ID TODO Убрать в ивент

        //Классs для работы с cообщениями и списком друзей и с лонгпул сервом
        MessagesArea messagesArea = new MessagesArea();
        FriendsArea friendsArea = new FriendsArea();
        LongPoolArea longPoolArea = new LongPoolArea();

        //Таймер для вызова обновления сообщений
        DispatcherTimer messagesRefreshTimer = new DispatcherTimer();

        public MainWindow()
        {
            authWindow = new Auth(vkApi, this);


            if (!isDebug)
            {
                this.Hide();
                authWindow.ShowDialog();
            }
            //Попадаем сюда только когда закрыли authWindow (Костыль) TODO 
            
            InitializeComponent();

            

            if (isDebug)
            {
                vkApi.Authorize(new ApiAuthParams
                {
                    ApplicationId = 5786604,
                    Login = "kostik2141@yandex.ru",
                    Password = "HJ9JYJOUau8",
                    Settings = Settings.All
                });
            }

            //Инициализируем наши зоны
            longPoolArea.Initialize(vkApi, friendsArea, messagesArea);
            messagesArea.Initialize(vkApi, longPoolArea, textBox_message, listBox_messages);
            friendsArea.Initialize(vkApi, listBox_friends);

            //Устанавливаем источник для нашего списка юзеров
            listBox_friends.ItemsSource = friendsArea.GetListOfFriendsBoxes();
            //И источник для списка сообщений
            listBox_messages.ItemsSource = messagesArea.GetListOfMessageBoxes();


            if (vkApi.IsAuthorized)
            {
                friendsArea.UpdateFriends();
            }


            //Устанавливаем таймер
            messagesRefreshTimer.Interval = TimeSpan.FromSeconds(1);
            messagesRefreshTimer.Tick += messagesRefreshTimer_Tick;
            messagesRefreshTimer.Start();
        }


      

        /*&&&&&$$         Event Handlers            &&&&&&&*/

        /// <summary>
        /// Sending a message when Enter pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_message_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                messagesArea.SendMessageFromBox();
            }
        }

        private void textBox_message_GotFocus(object sender, RoutedEventArgs e)
        {
            MessagesArea.OnGotFocusSetStyle(textBox_message);
        }

        private void textBox_message_LostFocus(object sender, RoutedEventArgs e)
        {
            MessagesArea.OnLostFocusSetStyle(textBox_message);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            messagesArea.SendMessageFromBox();
        }

        // Задаем выбранный в левом меню ID поле choosenIndex
        private void listBox_friends_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //TODO сменить на ивент
            choosenId = friendsArea.GetChoosenFriendId();
            messagesArea.SetChoosenId(choosenId);
            messagesArea.UpdateMessagesFullUpdate(choosenId);
        }

        //TODO
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            
        }

        void messagesRefreshTimer_Tick(object sender, EventArgs e)
        {
            longPoolArea.LoadUpdatesFromServer();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            authWindow.Close();
        }
    }
}
